﻿using UnityEngine;
using System.Collections;

public class TutorialScript : MonoBehaviour {

	GameObject tip1;
	GameObject tip2;
	GameObject tip3;
	GameObject tip4;
	GameObject tip5;
	GameObject tip6;
	GameObject tip7;
	GameObject tip8;
	GameObject tip9;
	GameObject tip10;
	GameObject tip11;
	GameObject tip12;

	//countdown
	public int countdown; //60(frames) * 10(seconds) * 7(tips)
	public int tips = 12;
	public int currentSlide = 1;
	public int delay = 10;
	// Use this for initialization
	void Start () {
		countdown = 60 * tips * delay;
		tip1 = GameObject.Find ("tip1");
		tip2 = GameObject.Find ("tip2");
		tip3 = GameObject.Find ("tip3");
		tip4 = GameObject.Find ("tip4");
		tip5 = GameObject.Find ("tip5");
		tip6 = GameObject.Find ("tip6");
		tip7 = GameObject.Find ("tip7");
		tip8 = GameObject.Find ("tip8");
		tip9 = GameObject.Find ("tip9");
		tip10 = GameObject.Find ("tip10");
		tip11 = GameObject.Find ("tip11");
		tip12 = GameObject.Find ("tip12");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		countdown--;

		//disabling the images
		if (countdown < (60 * delay * (tips - 1)))
		{
			if (tip1.renderer.enabled == true)
			{
				tip1.renderer.enabled = false;
				currentSlide = 2;
			}
		}
		if (countdown < (60 * delay * (tips - 2)))
		{
			if (tip2.renderer.enabled == true)
			{
				tip2.renderer.enabled = false;
				currentSlide = 3;
			}
		}
		if (countdown < (60 * delay * (tips - 3)))
		{
			if (tip3.renderer.enabled == true)
			{
				tip3.renderer.enabled = false;
				currentSlide = 4;
			}
		}
		if (countdown < (60 * delay * (tips - 4)))
		{
			if (tip4.renderer.enabled == true)
			{
				tip4.renderer.enabled = false;
				currentSlide = 5;
			}
		}
		if (countdown < (60 * delay * (tips - 5)))
		{
			if (tip5.renderer.enabled == true)
			{
				tip5.renderer.enabled = false;
				currentSlide = 6;
			}
		}
		if (countdown < (60 * delay * (tips - 6)))
		{
			if (tip6.renderer.enabled == true)
			{
				tip6.renderer.enabled = false;
				currentSlide = 7;
			}
		}
		if (countdown < (60 * delay * (tips - 7)))
		{
			if (tip7.renderer.enabled == true)
			{
				tip7.renderer.enabled = false;
				currentSlide = 8;
			}
		}
		if (countdown < (60 * delay * (tips - 8)))
		{
			if (tip8.renderer.enabled == true)
			{
				tip8.renderer.enabled = false;
				currentSlide = 9;
			}
		}
		if (countdown < (60 * delay * (tips - 9)))
		{
			if (tip9.renderer.enabled == true)
			{
				tip9.renderer.enabled = false;
				currentSlide = 10;
			}
		}
		if (countdown < (60 * delay * (tips - 10)))
		{
			if (tip10.renderer.enabled == true)
			{
				tip10.renderer.enabled = false;
				currentSlide = 11;
			}
		}
		if (countdown < (60 * delay * (tips - 11)))
		{
			if (tip11.renderer.enabled == true)
			{
				tip11.renderer.enabled = false;
				currentSlide = 12;
			}
		}
		if (countdown < (60 * delay * (tips - 12)))
		{
			Application.LoadLevel ("level1");
		}
	}
}
