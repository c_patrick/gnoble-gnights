﻿using UnityEngine;
using System.Collections;

public class CheckPointScript : MonoBehaviour {

	// player
	PlayerScript player;
	GUIindicator guiIndicator;
	BlossomHealthBarScript princessHealth;

	// Use this for initialization
	void Start () {
		princessHealth = GameObject.FindObjectOfType (typeof(BlossomHealthBarScript)) as BlossomHealthBarScript;
		player = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Destroy(gameObject);
			saveGame();
			print ("Game Saved");
		}
	}

	void saveGame()
	{
		guiIndicator.currentLevel = Application.loadedLevelName;
		guiIndicator.playerSeeds = player.seedCount;
		guiIndicator.playerHealth = player.health;
		guiIndicator.checkPointX = this.transform.position.x;
		guiIndicator.checkPointY = player.transform.position.y;
		guiIndicator.checkPointUsed = true;
		guiIndicator.playerDamage = player.damage;
		guiIndicator.princessHealth = princessHealth.currentHealth;
	}
}
