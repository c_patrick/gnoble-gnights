﻿using UnityEngine;
using System.Collections;

public class playerHitBoxScript : MonoBehaviour {

	private PlayerScript playerScript;
	public bool collision = true;

	GUIindicator guiIndicator;

	// Use this for initialization
	void Start () 
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	
	void OnTriggerEnter2D(Collider2D enemy)
	{
		if (enemy.tag == "Key") {
			// allow access to the prison cell
			GameObject.Find("PrisonCell").GetComponent<PrisonCellScript>().keyObtained = true;
			Destroy(enemy.gameObject);
		}

		if (enemy.name == "PrisonCell") {
			var prisonCell = GameObject.Find("PrisonCell").GetComponent<PrisonCellScript>();
			if (prisonCell.keyObtained) {
				prisonCell.GetComponent<Animator>().SetBool("open", true);
				guiIndicator.victory = true;
			}
		}

		if (playerScript.hitCooldown < 0 && collision)
		{

			if (playerScript.health > 0) {
				SoundEffectScript.Instance.PlayerHitSound();
			}

			//dealing damage based on enemy type
			if (enemy.name == "Creeper") {
				if (enemy.GetComponent<CreeperScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<CreeperScript>().damage;
					Bounce(enemy);
				}
			}
			else if (enemy.name == "MuncherAttackBox")
			{
				print ("HitByMuncher");
				if (enemy.transform.parent.GetComponent<MuncherScript>() != null)
				{
					print ("Dealing damage");
					playerScript.health -= enemy.transform.parent.GetComponent<MuncherScript>().attackDamage;
					Bounce(enemy);
				}
			}
			else if (enemy.name == "Muncher") {
				if (enemy.GetComponent<MuncherScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<MuncherScript>().damage;
					Bounce(enemy);
				}
			}
			else if (enemy.name == "Sunflower") {
				if (enemy.GetComponent<SunShooterScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<SunShooterScript>().damage;
					Bounce(enemy);
				}
			}
			else if (enemy.name == "seedShot(Clone)")
			{
				if (enemy.GetComponent<ShotScript>() != null)
				{
					if (enemy.GetComponent<ShotScript>() != null)
					{
					playerScript.health -= enemy.GetComponent<ShotScript>().damage;
					DestroyObject (enemy.gameObject);
					Bounce(enemy);
					}
				}
			}
			else if (enemy.name == "TulipAttackBox") {
				if (enemy.GetComponent<TulipScript>() != null)
				{
					playerScript.health -= enemy.transform.parent.GetComponent<TulipScript>().damage;
					Bounce(enemy);
				}
			}
			else if (enemy.name == "Tulip") {
				if (enemy.GetComponent<TulipScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<TulipScript>().damage;
					Bounce(enemy);
				}
			}
			// boss level
			// collide with mini boss
			else if (enemy.tag == "BossMini") {
				if (enemy.GetComponent<BossMiniScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<BossMiniScript>().damage;
					// destroy mini boss
					Destroy(enemy.gameObject);
					Bounce(enemy);
				}
			}
			else if (enemy.tag == "BossVine") {
				if (enemy.GetComponent<VineAttackScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<VineAttackScript>().damage;
					// make them bounce
					enemy.gameObject.layer = 20;
					enemy.gameObject.rigidbody2D.gravityScale = 2;
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));

					Bounce(enemy);
				}
			}
			else if (enemy.tag == "BossVineAttack") {
				if (enemy.GetComponent<VineAttackScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<VineAttackScript>().damage;
					// make them bounce
					enemy.gameObject.layer = 20;
					enemy.gameObject.rigidbody2D.gravityScale = 2;
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));
					
					// destroy vine
					Destroy(enemy.gameObject, 1);
					Bounce(enemy);
				}
			}
			else if (enemy.tag == "BossThorn") {
				if (enemy.GetComponent<ThornAttackScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<ThornAttackScript>().damage;
					Bounce(enemy);
				}
			}
			else if (enemy.tag == "Boss") {
				if (enemy.GetComponent<BossScript>() != null)
				{
					playerScript.health -= enemy.GetComponent<BossScript>().damage;
					Bounce(enemy);
				}
			}
		}
	}


	void Bounce(Collider2D enemy)
	{
		//making player jump back in the appropriate direction
		if (enemy.transform.position.x > playerScript.transform.position.x)
		{
			playerScript.transform.rigidbody2D.velocity = new Vector2(0, 0);
			if (playerScript.jumpDelay < 0)
				playerScript.transform.rigidbody2D.AddForce (new Vector2 (-100, 280));
			else
				playerScript.transform.rigidbody2D.AddForce (new Vector2 (-100, 0));
			playerScript.hitCooldown = 30;
		}
		else
		{
			playerScript.transform.rigidbody2D.velocity = new Vector2(0, 0);
			if (playerScript.jumpDelay < 0)
				playerScript.transform.rigidbody2D.AddForce (new Vector2 (100, 280));
			else
				playerScript.transform.rigidbody2D.AddForce (new Vector2 (100, 0));
			playerScript.hitCooldown = 30;
		}
	}
}
