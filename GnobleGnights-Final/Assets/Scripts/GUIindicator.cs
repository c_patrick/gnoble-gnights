﻿using UnityEngine;
using System.Collections;

public class GUIindicator : MonoBehaviour {

	//menu states
	public bool paused = false;
	public bool confirm = false;
	public bool options = false;
	public bool mainMenu = true;
	public bool inGame = false;

	//settings
	public bool sound = true;
	public bool music = true;

	public AudioClip audioClip;

	//gnome indicator
	public int activeGnome = 1;

	//menu cooldowns
	public int menuCooldown = 30;

	//game status
	public bool victory = false;
	public bool defeat = false;

	//player progress/saves
	public int playerSeeds = 0;
	public int playerHealth = 0;
	public int playerDamage = 1;
	public float checkPointX = 0f;
	public float checkPointY = 0f;
	public string currentLevel = "level1";
	public bool checkPointUsed = false;	

	public float princessHealth = 0;

	//initiating
	void Start()
	{

		//checking if a guiIndicator already exists first
		if ((GameObject.FindObjectsOfType (typeof(GUIindicator))).Length > 1)
		{
			Destroy(this.gameObject);
		}

		//loading saves
		if (PlayerPrefs.GetString ("level") == "level1" || PlayerPrefs.GetString ("level") == "level2" ||
			PlayerPrefs.GetString ("level") == "level3" || PlayerPrefs.GetString ("level") == "levelBoss")
		{
			currentLevel = PlayerPrefs.GetString ("level");
			if (PlayerPrefs.GetInt ("damage") > 0)
				playerDamage = PlayerPrefs.GetInt ("damage");
			else
				playerDamage = 1;
			if (PlayerPrefs.GetInt ("gnome") > 0)
				activeGnome = PlayerPrefs.GetInt ("gnome");
			else 
				activeGnome = 1;
			if (PlayerPrefs.GetInt ("sound") == 1)
				sound = true;
			else
				sound = false;
			if (PlayerPrefs.GetInt ("music") == 1)
				music = true;
			else
				music = false;
		}
		else
		{
			currentLevel = "gameIntro";
		}

		paused = false;
		confirm = false;
		options = false;
		mainMenu = true;
		inGame = false;
		victory = false;
		defeat = false;
		menuCooldown = 0;
		//read in player progress from file
	}

	void Awake() 
	{
		DontDestroyOnLoad (transform.gameObject);
	}

	void Update()
	{
		if (Application.loadedLevelName != "levelBoss")
		{
			if (music)
				this.audio.mute = false;
			else
				this.audio.mute = true;
		}
		else
		{
			if (GameObject.Find ("Boss").GetComponent<BossScript>().health <= 0 && music)
				this.audio.mute = false;
			else
				this.audio.mute = true;
		}
	}
}
