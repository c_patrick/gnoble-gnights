﻿using UnityEngine;
using System.Collections;

public class playerAttackBoxScript : MonoBehaviour {

	private PlayerScript playerScript;
	private BossScript bossScript;

	GUIindicator guiIndicator;

	// Use this for initialization
	void Start () 
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		bossScript = GameObject.FindObjectOfType (typeof(BossScript)) as BossScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	
	// Update is called once per frame
	void Update () {
	
		//enabling the attack bounding box
		if (playerScript.attack1 || playerScript.attack2)
		{
			this.transform.GetComponent<CircleCollider2D>().enabled = true;
		}
		else
			this.transform.GetComponent<CircleCollider2D>().enabled = false;
	}

	void OnTriggerEnter2D(Collider2D enemy)
	{
		SoundEffectScript.Instance.PlayerAttackSound();
		if (enemy.name == "Creeper")
		{
			if (enemy.GetComponent<CreeperScript>() != null)
			{
				if (enemy.GetComponent<CreeperScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));
					enemy.GetComponent<CreeperScript>().hitCooldown = 20;
					enemy.GetComponent<CreeperScript>().health -= playerScript.damage;

					//interupting their attack
					enemy.GetComponent<CreeperScript>().anim.SetTrigger ("hit");
				}
			}
		}
		else if (enemy.name == "Muncher")
		{
			if (enemy.GetComponent<MuncherScript>() != null)
			{
				if (enemy.GetComponent<MuncherScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (100, 280));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-100, 280));
					enemy.GetComponent<MuncherScript>().hitCooldown = 10;
					enemy.GetComponent<MuncherScript>().health -= playerScript.damage;
					
					//interupting their attack
					//enemy.GetComponent<MuncherScript>().anim.SetTrigger ("hit");
				}
			}
		}
		else if (enemy.name == "Sunflower")
		{
			if (enemy.GetComponent<SunShooterScript>() != null)
			{
				if (enemy.GetComponent<SunShooterScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (100, 280));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-100, 280));
					enemy.GetComponent<SunShooterScript>().hitCooldown = 10;
					enemy.GetComponent<SunShooterScript>().health -= playerScript.damage;
				}
			}
		}
		else if (enemy.name == "seedShot(Clone)")
		{
			if (enemy.GetComponent<ShotScript>() != null)
			{
				if (enemy.GetComponent<ShotScript>().cooldown < 0)
				{
					enemy.GetComponent<ShotScript>().Flip ();
					enemy.GetComponent<ShotScript>().direction *= -1;
					enemy.GetComponent<ShotScript>().cooldown = 20;
				}
				//DestroyObject (enemy.gameObject);
			}
		}
		else if (enemy.name == "Tulip")
		{
			if (enemy.GetComponent<TulipScript>() != null)
			{
				if (enemy.GetComponent<TulipScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));
					enemy.GetComponent<TulipScript>().hitCooldown = 20;
					enemy.GetComponent<TulipScript>().health -= playerScript.damage;
					
					//interupting their attack
					enemy.GetComponent<TulipScript>().anim.SetTrigger ("hit");
				}
			}
		}
		// boss level
		else if (enemy.tag == "BossMini") {
			// create attack effect
			SpecialEffectsHelper.Instance.AttackEffect(enemy.transform.position);
			
			bossScript.health -= 1;
			bossScript.transform.GetComponent<Animator>().SetTrigger("damaged");
			// destroy mini boss
			Destroy(enemy.gameObject);
		}
		else if (enemy.tag == "BossVine") {
			// create attack effect
			SpecialEffectsHelper.Instance.AttackEffect(enemy.transform.position);
			// make them bounce
			enemy.gameObject.layer = 20;
			enemy.gameObject.rigidbody2D.gravityScale = 2;
			if (this.transform.position.x < enemy.transform.position.x)
				enemy.rigidbody2D.AddForce (new Vector2 (250, 200));
			else
				enemy.rigidbody2D.AddForce (new Vector2 (-250, 200));
			
			bossScript.health -= 1;
			bossScript.transform.GetComponent<Animator>().SetTrigger("damaged");
			Destroy(enemy.gameObject, 2);
		}
		else if (enemy.tag == "BossVineAttack") {
			// create attack effect
			SpecialEffectsHelper.Instance.AttackEffect(enemy.transform.position);
			// make them bounce
			enemy.gameObject.layer = 20;
			enemy.gameObject.rigidbody2D.gravityScale = 2;
			if (this.transform.position.x < enemy.transform.position.x)
				enemy.rigidbody2D.AddForce (new Vector2 (250, 200));
			else
				enemy.rigidbody2D.AddForce (new Vector2 (-250, 200));
			
			bossScript.health -= 1;
			bossScript.transform.GetComponent<Animator>().SetTrigger("damaged");
			Destroy(enemy.gameObject, 2);
		}
		else if (enemy.tag == "BossThorn") {
			// create attack effect
			SpecialEffectsHelper.Instance.AttackEffect(enemy.transform.position);
			// make them bounce
			enemy.gameObject.rigidbody2D.gravityScale = 1;
			enemy.rigidbody2D.velocity = new Vector2(0, 0);
			if (this.transform.position.x < enemy.transform.position.x)
				enemy.rigidbody2D.AddForce (new Vector2 (250, 200));
			else
				enemy.rigidbody2D.AddForce (new Vector2 (-250, 200));
		}
	}
}
