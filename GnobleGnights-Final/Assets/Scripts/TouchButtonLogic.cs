﻿using UnityEngine;
using System.Collections;

//specifically for the in-game gui
public class TouchButtonLogic : MonoBehaviour {

	private PlayerScript playerScript;
	private PauseControl pauseScript;
	private GUIindicator guiIndicator;
	private BlossomHealthBarScript princessHealth;

	void Start()
	{
		princessHealth = GameObject.FindObjectOfType (typeof(BlossomHealthBarScript)) as BlossomHealthBarScript;
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		pauseScript = GameObject.FindObjectOfType (typeof(PauseControl)) as PauseControl;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;

		guiIndicator.defeat = false;
		guiIndicator.mainMenu = false;
		guiIndicator.inGame = true;
		guiIndicator.options = false;
		guiIndicator.paused = false;
		guiIndicator.confirm = false;
	}
	
	void Update () 
	{
		//updating the cooldown
		guiIndicator.menuCooldown --;

		//is the game pause?
		//if so enable the textures of the pause menu
		if (guiIndicator.paused)
		{
			if (this.name.Equals ("mainMenuButton") || this.name.Equals ("optionsButton") || this.name.Equals ("resumeButton") || this.name.Equals ("pauseBackground"))
				this.guiTexture.enabled = true;
		}
		else
		{
			if (this.name.Equals ("mainMenuButton") || this.name.Equals ("optionsButton") || this.name.Equals ("resumeButton") || this.name.Equals ("pauseBackground"))
				this.guiTexture.enabled = false;
		}

		//is the confirmation window open
		//if so enable the textures for the confirmation window
		if (guiIndicator.confirm)
		{
			if (this.name.Equals ("sure") || this.name.Equals ("yes") || this.name.Equals ("no"))
				this.guiTexture.enabled = true;
		}
		else
		{
			if (this.name.Equals ("sure") || this.name.Equals ("yes") || this.name.Equals ("no"))
				this.guiTexture.enabled = false;
		}

		//displaying options menu if it is activated
		if (guiIndicator.options)
		{
			if (this.name.Equals ("return"))
				this.guiTexture.enabled = true;
			else if (this.name.Equals ("musicOn"))
			{
				if (guiIndicator.music)
				{
					this.guiTexture.enabled = true;
					GameObject.Find ("musicOff").guiTexture.enabled = false;
				}
				else
				{
					this.guiTexture.enabled = false;
					GameObject.Find ("musicOff").guiTexture.enabled = true;
				}
			}
			else if (this.name.Equals ("musicOff"))
			{
				if (!guiIndicator.music)
				{
					this.guiTexture.enabled = true;
					GameObject.Find ("musicOn").guiTexture.enabled = false;
				}
				else
				{
					this.guiTexture.enabled = false;
					GameObject.Find ("musicOn").guiTexture.enabled = true;
				}
			}
			else if (this.name.Equals ("soundOn"))
			{
				if (guiIndicator.sound)
				{
					this.guiTexture.enabled = true;
					GameObject.Find ("soundOff").guiTexture.enabled = false;
				}
				else
				{
					this.guiTexture.enabled = false;
					GameObject.Find ("soundOff").guiTexture.enabled = true;
				}
			}
			else if (this.name.Equals ("soundOff"))
			{
				if (!guiIndicator.sound)
				{
					this.guiTexture.enabled = true;
					GameObject.Find ("soundOn").guiTexture.enabled = false;
				}
				else
				{
					this.guiTexture.enabled = false;
					GameObject.Find ("soundOn").guiTexture.enabled = true;
				}
			}
		}
		else if (guiIndicator.options == false)
		{
			if (this.name.Equals ("musicOn") || this.name.Equals ("musicOff") || this.name.Equals ("soundOff") || this.name.Equals ("soundOn") || this.name.Equals ("return"))
				this.guiTexture.enabled = false;
		}

		//end-game menu (defeat)
		if (guiIndicator.defeat)
		{
			if (this.name.Equals ("endBackground") || this.name.Equals ("playAgain") || this.name.Equals ("yesEnd") || this.name.Equals ("noEnd"))
				this.guiTexture.enabled = true;
		}
		else
		{
			if (this.name.Equals ("endBackground") || this.name.Equals ("playAgain") || this.name.Equals ("yesEnd") || this.name.Equals ("noEnd"))
				this.guiTexture.enabled = false;
		}

		//is there a touch on screen?
		if (Input.touches.Length > 0)
		{
			//loop through all the toucheson screen
			for (int i = 0; i < Input.touchCount; i++)
			{
				//executes this code for the current touch (i) on screen
				if (this.guiTexture.HitTest(Input.GetTouch (i).position))
				{
					string name = this.name;
					Debug.Log (name);

					//code for buttons that must register continous touch
					if (guiIndicator.paused) 
					{}
					else
					{
						//movement buttons (active if touched)
						if (name.Equals ("left"))
							playerScript.leftPressed ();
						else if (name.Equals ("right"))
							playerScript.rightPressed ();
					}

					//if current touch hits our gui texture, run this code
					if (Input.GetTouch(i).phase == TouchPhase.Began)
					{
						if (guiIndicator.paused)
						{
							if (name.Equals ("pause"))
							{
								if (guiIndicator.options)
								{
									guiIndicator.options = false;
									guiIndicator.menuCooldown = 30;
								}
								else if (guiIndicator.confirm)
								{
									guiIndicator.confirm = false;
									guiIndicator.menuCooldown = 30;
								}
								else
								{
									guiIndicator.paused = false;
									pauseScript.UnpauseGame ();
								}
							}
						}
						else
						{
							if (name.Equals ("attack1"))
								playerScript.attack1Pressed ();

							else if (name.Equals ("attack2Button") && (guiTexture.texture.name.Equals("attack2ButtonEnabled")))
								playerScript.attack2Pressed ();

							else if (name.Equals ("jump"))
								playerScript.jumpPressed ();

							else if (name.Equals ("pause"))
							{
								pauseScript.PauseGame();
								guiIndicator.paused = true;
							}
						}
					}

					//letting go of direction keys
					if (Input.GetTouch (i).phase == TouchPhase.Ended)
					{
						//end game menu
						if (guiIndicator.defeat || guiIndicator.victory)
						{
							//insert some special behaviour if victory state

							if (name.Equals ("yesEnd"))
							{
								guiIndicator.defeat = false;
								guiIndicator.mainMenu = false;
								guiIndicator.inGame = true;
								guiIndicator.options = false;
								guiIndicator.paused = false;
								guiIndicator.confirm = false;
								// loaded saved data
								loadSaved();

							}
							else if (name.Equals ("noEnd"))
							{
								guiIndicator.mainMenu = true;
								guiIndicator.inGame = false;
								guiIndicator.options = false;
								guiIndicator.paused = false;
								guiIndicator.confirm = false;
								guiIndicator.defeat = false;
								PlayerPrefs.SetString ("level", Application.loadedLevelName);
								PlayerPrefs.SetInt ("damage", guiIndicator.playerDamage);
								PlayerPrefs.SetInt ("gnome", guiIndicator.activeGnome);
								Application.LoadLevel ("main"); //should save progress as those given for level restart
							}
						}

						else if (!guiIndicator.paused)
						{
							if (name.Equals ("left") || name.Equals ("right"))
								playerScript.movementReleased();
						}

						//checking the textures for the pause menu
						else
						{
							if (guiIndicator.menuCooldown < 0)
							{
								//checking for confirmation menu input
								if (guiIndicator.confirm)
								{
									if (name.Equals ("yes"))
									{
										pauseScript.UnpauseGame ();
										guiIndicator.confirm = false;
										guiIndicator.options = false;
										guiIndicator.defeat = false;
										guiIndicator.menuCooldown = 30;
										PlayerPrefs.SetString ("level", Application.loadedLevelName);
										PlayerPrefs.SetInt ("damage", guiIndicator.playerDamage);
										PlayerPrefs.SetInt ("gnome", guiIndicator.activeGnome);
										Application.LoadLevel ("main");
									}
									else if (name.Equals ("no"))
									{
										guiIndicator.confirm = false;
										guiIndicator.menuCooldown = 30;
									}
								}
								//checking for options menu input
								else if (guiIndicator.options)
								{
									if (name.Equals ("musicOn"))
									{
										guiIndicator.music = !guiIndicator.music;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("musicOff"))
									{
										guiIndicator.music = !guiIndicator.music;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("soundOn"))
									{
										guiIndicator.sound = !guiIndicator.sound;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("soundOff"))
									{
										guiIndicator.sound = !guiIndicator.sound;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("return"))
									{
										guiIndicator.options = false;
										guiIndicator.menuCooldown = 30;
									}
								}
								//checking for standard pause menu input
								else
								{
									if (name.Equals ("resumeButton"))
									{
										pauseScript.UnpauseGame ();
										guiIndicator.paused = false;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("mainMenuButton"))
									{
										guiIndicator.confirm = true;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("optionsButton"))
									{
										guiIndicator.options = true;
										guiIndicator.menuCooldown = 30;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void loadSaved()
	{
		//reseting the defeat variable
		guiIndicator.defeat = false;
		guiIndicator.victory = false;

		if (Application.loadedLevelName == "levelBoss")
		{
			guiIndicator.defeat = false;
			guiIndicator.victory = false;
			Application.LoadLevel ("levelBoss");
		}

		//reseting the player position/seeds/princess health/health
		else if (princessHealth.currentHealth <= 0)
		{
			guiIndicator.defeat = false;
			guiIndicator.playerDamage = playerScript.damage;
			Application.LoadLevel (Application.loadedLevelName);
		}
		if (guiIndicator.checkPointUsed)
		{
			playerScript.health = guiIndicator.playerHealth;
			Vector2 respawnPoint = new Vector2 (guiIndicator.checkPointX, guiIndicator.checkPointY + 3);
			playerScript.transform.position = respawnPoint;
			playerScript.rigidbody2D.velocity = new Vector2 (0, 0);
			princessHealth.currentHealth = guiIndicator.princessHealth;
		}
		//if checkpoint hasn't been used then reset to the defaults at the start of the level
		else
		{
			if (Application.loadedLevelName == "level1")
			{
				playerScript.health = 6;
				playerScript.seedCount = guiIndicator.playerSeeds;
				playerScript.damage = 1;
				Vector2 respawnPoint = new Vector2 (-7.1f, -0.14f);
				playerScript.transform.position = respawnPoint;
				playerScript.rigidbody2D.velocity = new Vector2 (0, 0);
				princessHealth.currentHealth = 180;
			}
			else if (Application.loadedLevelName == "level2")
			{
				playerScript.health = 6;
				playerScript.damage = 2;
				Vector2 respawnPoint = new Vector2 (-4.7f, -0.45f);
				playerScript.transform.position = respawnPoint;
				playerScript.rigidbody2D.velocity = new Vector2 (0, 0);
				princessHealth.currentHealth = 300;
			}
			else if (Application.loadedLevelName == "level3")
			{
				playerScript.health = 6;
				playerScript.damage = 3;
				Vector2 respawnPoint = new Vector2 (-1.19f, 0.39f);
				playerScript.transform.position = respawnPoint;
				playerScript.rigidbody2D.velocity = new Vector2 (0, 0);
				princessHealth.currentHealth = 420;
			}
			else if (Application.loadedLevelName == "levelBoss")
			{
				playerScript.health = 6;
				playerScript.damage = 4;
				Vector2 respawnPoint = new Vector2 (-8.75f, 2.32f);
				playerScript.transform.position = respawnPoint;
				playerScript.rigidbody2D.velocity = new Vector2 (0, 0);
			}
		}

		//re-enabling player sprites
		if (guiIndicator.activeGnome == 1)
		{
			playerScript.transform.FindChild ("backarm1").renderer.enabled = true;
			playerScript.transform.FindChild ("legs1").renderer.enabled = true;
			playerScript.transform.FindChild ("torso1").renderer.enabled = true;
		}
		else if (guiIndicator.activeGnome == 2)
		{
			playerScript.transform.FindChild ("backarm2").renderer.enabled = true;
			playerScript.transform.FindChild ("legs2").renderer.enabled = true;
			playerScript.transform.FindChild ("torso2").renderer.enabled = true;
		}
		else
		{
			playerScript.transform.FindChild ("backarm3").renderer.enabled = true;
			playerScript.transform.FindChild ("legs3").renderer.enabled = true;
			playerScript.transform.FindChild ("torso3").renderer.enabled = true;
		}

		//re-enabling player script
		playerScript.enabled = true;
	}
}
