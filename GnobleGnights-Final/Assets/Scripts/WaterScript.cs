﻿using UnityEngine;
using System.Collections;

public class WaterScript : MonoBehaviour {

	GUIindicator guiIndicator;

	// Use this for initialization
	void Start () {
		//guiIndicator
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "Player")
		{
			SoundEffectScript.Instance.SplashSound();
			other.GetComponent<PlayerScript>().transform.FindChild ("backarm1").renderer.enabled = false;
			other.GetComponent<PlayerScript>().transform.FindChild ("legs1").renderer.enabled = false;
			other.GetComponent<PlayerScript>().transform.FindChild ("torso1").renderer.enabled = false;
			guiIndicator.defeat = true;
			//other.enabled = false;
		}
	}
}
