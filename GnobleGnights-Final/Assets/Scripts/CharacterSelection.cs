﻿using UnityEngine;
using System.Collections;

//specifially for the character selection scene
public class CharacterSelection : MonoBehaviour {

	GUIindicator guiIndicator;

	void Start()
	{
		//guiIndicator
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	void Update () 
	{
		//is there a touch on screen?
		if (Input.touches.Length <= 0)
		{
			//if no touches then execute this code
			
		}
		else //if there is a touch
		{
			//loop through all the toucheson screen
			for (int i = 0; i < Input.touchCount; i++)
			{
				//executes this code for the current touch (i) on screen
				if (this.guiTexture.HitTest(Input.GetTouch (i).position))
				{
					string name = this.name;
					
					//code here to change appearance of button if it is being touched
					
					//if current touch hits our gui texture, run this code
					if (Input.GetTouch(i).phase == TouchPhase.Began)
					{
						//going to need to change a setting telling the next level which character to load (probably going to have to have a central game data object)
						//should load story introduction level instead of game level, but that comes later
						if (name.Equals ("gnome1"))
						{
							guiIndicator.mainMenu = false;
							guiIndicator.inGame = true;
							guiIndicator.options = false;
							guiIndicator.paused = false;
							guiIndicator.confirm = false;
							guiIndicator.activeGnome = 1;
							Application.LoadLevel ("tutorialLevel");
						}
						if (name.Equals ("gnome2"))
						{
							guiIndicator.mainMenu = false;
							guiIndicator.inGame = true;
							guiIndicator.options = false;
							guiIndicator.paused = false;
							guiIndicator.confirm = false;
							guiIndicator.activeGnome = 2;
							Application.LoadLevel ("tutorialLevel");
						}
						if (name.Equals ("gnome3"))
						{
							guiIndicator.mainMenu = false;
							guiIndicator.inGame = true;
							guiIndicator.options = false;
							guiIndicator.paused = false;
							guiIndicator.confirm = false;
							guiIndicator.activeGnome = 3;
							Application.LoadLevel ("tutorialLevel");
						}
					}
					
					//letting go of a button
					if (Input.GetTouch (i).phase == TouchPhase.Ended)
					{
						
					}
				}
			}
		}
	}
}
