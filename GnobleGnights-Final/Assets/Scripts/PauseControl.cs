﻿using UnityEngine;
using System.Collections;

public class PauseControl : MonoBehaviour {

	public void PauseGame()
	{
		Time.timeScale = 0;
	}
	
	public void UnpauseGame()
	{
		Time.timeScale = 1;
	}
}
