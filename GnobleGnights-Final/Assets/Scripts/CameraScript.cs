﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	void Awake()
	{
		camera.aspect = 16f / 9f;
	}

	// Update is called once per frame
	void Update () {
		Vector3 position = transform.position;
		position.y = 0.05f;
		transform.position = position;
	}
}
