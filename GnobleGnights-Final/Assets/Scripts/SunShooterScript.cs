﻿using UnityEngine;
using System.Collections;

public class SunShooterScript : MonoBehaviour {

	//stats
	public int health = 1;
	public int damage = 1;
	public int seedValue = 10;
	public float maxSpeed = 2f;
	public float sightRange = 7f; //2 for standard platforms, 5.5 for standard ground blocks (complete range - might want enemy view range to be smaller)
	public float attackRange = 7f; //will vary for every enemy (not every creeper), public so that it can be easily changed without opening the script editor
	
	//direction
	private float direction = -1f;
	
	//animator
	public Animator anim;
	
	//cooldown
	public int attackCooldown = 20;
	private int attackBoxCooldown = 5;
	public int dirChangeCooldown = 20;
	public int hitCooldown = 0;
	
	//playerObejct
	PlayerScript playerScript;

	//shot prefab
	public Transform shotPrefab;
	
	//player in sight
	public bool playerSpotted = false;
	
	// Use this for initialization
	void Start () {
		//initialise animator here
		anim = transform.Find ("SunflowerSprite").GetComponent<Animator> ();
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}
	
	//update here
	void FixedUpdate () {
		
		//updating cooldowns
		attackCooldown --;
		dirChangeCooldown --;
		hitCooldown --;
		attackBoxCooldown --;
		
		//changing layer if hit so they don't hit boundaries (so you can knock them off platforms)
		if (hitCooldown > 0)
			gameObject.layer = 14;
		else
			gameObject.layer = 12;
		
		//check if in range of attack
		if (Mathf.Abs (playerScript.transform.position.x - this.transform.position.x) < attackRange && Mathf.Abs ((playerScript.transform.position.y - 0.17f) - this.transform.position.y) < 1f)
		{
			if (attackCooldown < 0)
			{
				anim.SetTrigger("attack");
				attackBoxCooldown = 5;
				Attack ();
				attackCooldown = 70;
			}
		}
				
		//if the enemy is on screen then check for player distance
		if (transform.Find ("SunflowerSprite").renderer.isVisible) 
		{
			//if the player is within range of sight on the x axis
			if (Mathf.Abs(playerScript.transform.position.x - this.transform.position.x) < sightRange)
			{
				//if the player is within range of sight on the y axis
				if (Mathf.Abs ((playerScript.transform.position.y - 0.17f) - this.transform.position.y) < 1.28f)
				{
					//change direction to face player
					if (playerScript.transform.position.x < this.transform.position.x)
					{
						if (direction != -1)
						{
							direction = -1;
							Flip ();
						}
					}
					else
					{
						if (direction != 1)
						{
							direction = 1;
							Flip ();
						}
					}
					//set player spotted bool to true
					playerSpotted = true;
				}
				else
				{
					//set player spotted bool to false
					playerSpotted = false;
				}
			}
			else
			{
				//set player spotted bool to false
				playerSpotted = false;
			}
		}
		//destroying object if health is too low
		if (health <= 0 || transform.position.y < -4)
		{
			SeedEffectScript.Instance.SeedEffect(transform.position);
			DestroyObject (this);
			if (health <= 0)
				playerScript.seedCount += seedValue;
		}
	}
	
	public void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	public void changeDirection()
	{
		direction *= -1;
		Flip ();
		dirChangeCooldown = 20;
	}

	void Attack()
	{
		if (attackCooldown < 0)
		{
			var shotInstance = Instantiate (shotPrefab) as Transform;
			shotInstance.position = transform.position;
			shotInstance.GetComponent<ShotScript>().direction = this.direction;
			if (this.direction == 1)
				shotInstance.GetComponent<ShotScript>().Flip();
		}
	}
}
