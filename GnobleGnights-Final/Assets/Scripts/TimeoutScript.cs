﻿using UnityEngine;
using System.Collections;

public class TimeoutScript : MonoBehaviour {

	private int countdown = 300;
	public AudioClip audioClip;
	
	GUIindicator guiIndicator;
		
	// Use this for initialization
	void Start () {
		
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	void PlaySound()
	{
		if (guiIndicator.sound)
		{
			audio.clip = audioClip;
			audio.Play ();
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//PlaySound ();
		countdown--;

		if (countdown <= 0)
			Application.LoadLevel ("main");
	}
}
