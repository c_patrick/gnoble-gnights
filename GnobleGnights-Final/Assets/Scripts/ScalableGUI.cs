﻿using UnityEngine;
using System.Collections;

//note: this class does not take into account circles/squares, or the spaces between objects
public class ScalableGUI : MonoBehaviour 
{
	private GUITexture myGUITexture;

	void Awake()
	{
		myGUITexture = this.gameObject.GetComponent ("GUITexture") as GUITexture;
	}
	
	void Start () 
	{
		//getting screen dimensions
		int screenHeight = Screen.height;
		int screenWidth = Screen.width;

		//int textureWidth = myGUITexture.texture.width;
		//int textureHeight = myGUITexture.texture.height;

		//getting aspect ratios
		//int screenAspectRatio = (screenWidth / screenHeight);
		//int textureAspectRatio = (textureWidth / textureHeight);

		//scale textures based on new screen size
		float newWidth = myGUITexture.pixelInset.width * (screenWidth / 1280f);
		float newHeight = myGUITexture.pixelInset.height * (screenHeight / 720f);

		//applying the new values
		myGUITexture.pixelInset = new Rect(0, 0, newWidth, newHeight);
	}
}