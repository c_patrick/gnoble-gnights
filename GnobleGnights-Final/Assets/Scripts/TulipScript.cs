﻿using UnityEngine;
using System.Collections;

public class TulipScript : MonoBehaviour {

	//stats
	public int health = 3;
	public int damage = 1;
	public int seedValue = 10;
	public float maxSpeed = 3f; //maybe make them hop, pause, hop, etc
	public float sightRange = 5.5f; //2 for standard platforms, 5.5 for standard ground blocks (complete range - might want enemy view range to be smaller)
	public float attackRange = 1f; //will vary for every enemy (not every creeper), public so that it can be easily changed without opening the script editor
	
	//direction
	private float direction = -1;
	
	//animator
	public Animator anim;
	
	//cooldown
	public int attackCooldown = 40;
	private int attackBoxCooldown = 10;
	public int dirChangeCooldown = 20;
	public int hitCooldown = 0;
	public int hopCooldown = 20;



	//playerObejct
	PlayerScript playerScript;

	// heart prefab
	public Transform heartPrefab;
	
	//player in sight
	public bool playerSpotted = false;

	// Use this for initialization
	void Start () {
		//initialise animator here
		anim = transform.Find ("TulipSprite").GetComponent<Animator> ();
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}

	//update here
	void FixedUpdate () {

		//updating cooldowns
		attackCooldown --;
		dirChangeCooldown --;
		hitCooldown --;
		attackBoxCooldown --;
		hopCooldown --;

		//changing layer if hit so they don't hit boundaries (so you can knock them off platforms)
		if (hitCooldown >= 0)
			gameObject.layer = 14;
		else
			gameObject.layer = 12;

		//enabling/disabling attack boxes
		if (attackBoxCooldown >= 0 && attackBoxCooldown < 5 && hitCooldown < 0)
			transform.FindChild ("TulipAttackBox").GetComponent<CircleCollider2D> ().enabled = true;
		else
			transform.FindChild ("TulipAttackBox").GetComponent<CircleCollider2D> ().enabled = false;

		//check if in range of attack
		if (Mathf.Abs (playerScript.transform.position.x - this.transform.position.x) < attackRange && Mathf.Abs ((playerScript.transform.position.y - 0.2f) - this.transform.position.y) < 0.4f)
		{
			if (attackCooldown < 0)
			{
				anim.SetTrigger("attack");
				attackBoxCooldown = 20;
				attackCooldown = 30;
			}
		}
		//else move
		else
		{
			//don't move if falling
			if (transform.rigidbody2D.velocity.y == 0)
			{
				//if hit cooldown has expired
				if (hitCooldown < 0 && hopCooldown < 0)
				{
					//moving the creeper
					anim.SetTrigger ("hop");
					hopCooldown = 30;
					rigidbody2D.velocity = new Vector2 (direction * maxSpeed, rigidbody2D.velocity.y);
				}
			}
		}
		//if the enemy is on screen then check for player distance
		if (transform.Find ("TulipSprite").renderer.isVisible) 
		{
			//if the player is within range of sight on the x axis
			if (Mathf.Abs(playerScript.transform.position.x - this.transform.position.x) < sightRange)
			{
				//if the player is within range of sight on the y axis
				if (Mathf.Abs ((playerScript.transform.position.y - 0.17f) - this.transform.position.y) < 0.7f)
				{
					//change direction to face player
					if (playerScript.transform.position.x < this.transform.position.x)
					{
						if (direction != -1)
						{
							direction = -1;
							Flip ();
						}
					}
					else
					{
						if (direction != 1)
						{
							direction = 1;
							Flip ();
						}
					}
					//set player spotted bool to true
					playerSpotted = true;
				}
				else
				{
					//set player spotted bool to false
					playerSpotted = false;
				}
			}
			else
			{
				//set player spotted bool to false
				playerSpotted = false;
			}
		}
		//destroying object if health is too low
		if (health <= 0 || transform.position.y < -4)
		{
			SeedEffectScript.Instance.SeedEffect(transform.position);
			DestroyObject (this);
			if (health <= 0)
			{
				heart ();
			}

		}
	}

	public void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	public void changeDirection()
	{
		direction *= -1;
		Flip ();
		dirChangeCooldown = 80;
	}

	void heart()
	{
		//if (attackCooldown < 0)
		//{
			var heartInstance = Instantiate (heartPrefab) as Transform;
			heartInstance.position = transform.position;
			heartInstance.GetComponent<PickUp>();

		//}
	}
}
