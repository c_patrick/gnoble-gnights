﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	private GUIindicator guiIndicator;

	void Start()
	{
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Escape))
		{
			if (guiIndicator.options == true)
				guiIndicator.options = false;
			else
				guiIndicator.confirm = true;
		}
	}
}
