﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {

	//damage
	public int damage = 1;

	//speed
	public float speed = 3f;

	//direction
	public float direction = -1f;

	//movement
	private Vector2 movement;

	//reflection cooldown
	public int cooldown = 20;

	// Use this for initialization
	void Start () {
		//destroying game object after 15 seconds
		Destroy (gameObject, 8);
	}
	
	// Update is called once per frame
	void Update () {

		//movement
		movement = new Vector2 (speed * direction, 0);

	}

	void FixedUpdate()
	{
		cooldown--;

		rigidbody2D.velocity = movement;
	}

	public void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
