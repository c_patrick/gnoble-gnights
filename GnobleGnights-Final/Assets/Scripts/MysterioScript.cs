﻿using UnityEngine;
using System.Collections;

public class MysterioScript : MonoBehaviour {

	private PlayerScript playerScript;
	private GUIindicator guiIndicator;
	private GUITexture dialog;
	public Texture2D[] texture;
	private bool upgradePurchased = false;
	
	// Use this for initialization
	void Start () 
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player" && !upgradePurchased)
		{
			int cost = 200;
			
			if (Application.loadedLevelName.Equals ("level2"))
			{
				cost = 300;
			}
			else if (Application.loadedLevelName.Equals ("level3"))
			{
				cost = 400;
			}
			if (playerScript.seedCount >= cost)
			{
				transform.FindChild("Seeds").renderer.enabled = true;
				upgradePurchased = true;
				playerScript.seedCount -= cost;
				playerScript.damage ++;
				guiIndicator.playerDamage = playerScript.damage;
				Destroy(gameObject, 4);
			}
			else
			{
				transform.FindChild("noSeeds").renderer.enabled = true;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Player") {
			transform.FindChild("Seeds").renderer.enabled = false;
			transform.FindChild("noSeeds").renderer.enabled = false;
		}
	}
}
