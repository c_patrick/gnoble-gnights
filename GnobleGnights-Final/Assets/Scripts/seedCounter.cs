﻿using UnityEngine;
using System.Collections;

public class seedCounter : MonoBehaviour {

	private PlayerScript playerScript;
	
	// Use this for initialization
	void Start () 
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}
	
	// Update is called once per frame
	void Update () {

		GUIText seeds = this.GetComponent<GUIText> ();
		seeds.text = "" + playerScript.seedCount;
	}
}
