﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	//NB: when using the box collider for collision with enemies, 
	//have it in a separate game object in a separate layer that doesn't collide with anything it doesn't need to (i.e. platforms)

	//max speed
	public float maxSpeed = 10f;
	
	//direction
	public bool facingRight = true;

	//animator
	Animator animArm; 
	Animator animLegs; 
	Animator animTorso; 

	//on the ground?
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.25f;
	public LayerMask whatIsGround;

	//movement
	public float move;

	//jump force
	public float jumpForce = 700f;

	//cooldowns
	public int jumpDelay = 10;
	private int movementCountdown;
	private int attackCooldown;
	public int hitCooldown = 0;
	private int startDelay = 30;

	//health
	public int health = 6; //six halves makes default 3 hearts
	//damage
	public int damage = 1;
	//seeds
	public int seedCount = 0;

	//attacks
	public bool attack1 = false;
	public bool attack2 = false;
	private int attackBoxCooldown = 0;

	// special attack
	public GUITexture attack2Button;
	public Texture2D disabledTexture;

	// special attacks prefab
	SpecialAttackScript specialAttackS;
	public Transform lawnmowerPrefab = null;
	public Transform leafblowerPrefab = null;
	public Transform pitchforkPrefab = null;
	
	//main camera object
	CameraScript camera;

	//guiIndicator (should be gameInfo rather)
	GUIindicator guiIndicator;


	void Start()
	{
		//guiIndicator
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;

		//reseting guiIndicator
		guiIndicator.checkPointUsed = false;

		//damage
		damage = guiIndicator.playerDamage;

		specialAttackS = GameObject.FindObjectOfType (typeof(SpecialAttackScript)) as SpecialAttackScript;

		//setting up the animators and enabling sprites
		if (guiIndicator.activeGnome == 1)
		{
			animArm = transform.Find ("backarm1").GetComponent<Animator> ();
			animLegs = transform.Find ("legs1").GetComponent<Animator> ();
			animTorso = transform.Find ("torso1").GetComponent<Animator> ();
			//gnome 1 is visible by default
		}
		else if (guiIndicator.activeGnome == 2)
		{
			animArm = transform.Find ("backarm2").GetComponent<Animator> ();
			animLegs = transform.Find ("legs2").GetComponent<Animator> ();
			animTorso = transform.Find ("torso2").GetComponent<Animator> ();
			//disabling gnome  1
			transform.Find ("torso1").renderer.enabled = false;
			transform.Find ("backarm1").renderer.enabled = false;
			transform.Find ("legs1").renderer.enabled = false;
			//enabling gnome 2
			transform.Find ("torso2").renderer.enabled = true;
			transform.Find ("backarm2").renderer.enabled = true;
			transform.Find ("legs2").renderer.enabled = true;
		}
		else if (guiIndicator.activeGnome == 3)
		{
			animArm = transform.Find ("backarm3").GetComponent<Animator> ();
			animLegs = transform.Find ("legs3").GetComponent<Animator> ();
			animTorso = transform.Find ("torso3").GetComponent<Animator> ();
			//disabling gnome  1
			transform.Find ("torso1").renderer.enabled = false;
			transform.Find ("backarm1").renderer.enabled = false;
			transform.Find ("legs1").renderer.enabled = false;
			//enabling gnome 3
			transform.Find ("torso3").renderer.enabled = true;
			transform.Find ("backarm3").renderer.enabled = true;
			transform.Find ("legs3").renderer.enabled = true;

		}

		//camera
		camera = GameObject.FindObjectOfType (typeof(CameraScript)) as CameraScript;
	}

	//don't use delta time as timestep is fixed
	void FixedUpdate()
	{
		damage = guiIndicator.playerDamage;

		//checking for and setting defeat
		if (health <= 0)
		{
			guiIndicator.defeat = true;
			transform.FindChild ("backarm1").renderer.enabled = false;
			transform.FindChild ("legs1").renderer.enabled = false;
			transform.FindChild ("torso1").renderer.enabled = false;
			transform.FindChild ("backarm2").renderer.enabled = false;
			transform.FindChild ("legs2").renderer.enabled = false;
			transform.FindChild ("torso2").renderer.enabled = false;
			transform.FindChild ("backarm3").renderer.enabled = false;
			transform.FindChild ("legs3").renderer.enabled = false;
			transform.FindChild ("torso3").renderer.enabled = false;
			transform.rigidbody2D.velocity = new Vector2 (5, 0);
			this.enabled = false;
		}

		//are we on the ground?
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		animArm.SetBool ("Ground", grounded);
		animLegs.SetBool ("Ground", grounded);
		animTorso.SetBool ("Ground", grounded);

		//vertical speed for animation
		animArm.SetFloat ("vSpeed", rigidbody2D.velocity.y);
		animLegs.SetFloat ("vSpeed", rigidbody2D.velocity.y);
		animTorso.SetFloat ("vSpeed", rigidbody2D.velocity.y);

		//changing layer based on groundedness
		if (grounded)
			gameObject.layer = 8;
		else
			gameObject.layer = 9;

		//speed for animation
		animArm.SetFloat ("Speed", Mathf.Abs (move));
		animLegs.SetFloat ("Speed", Mathf.Abs (move));
		animTorso.SetFloat ("Speed", Mathf.Abs (move));

		//moving the player
		if (hitCooldown < 0 && !guiIndicator.defeat)
		{
			rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);
		}


		if (move > 0 && !facingRight)
			Flip ();
		else if (move < 0 && facingRight)
			Flip ();

		//decreasing cooldowns
		if (startDelay >= 0)
			startDelay--;

		jumpDelay--;

		movementCountdown--;
		if (movementCountdown < 0)
			move = 0;

		attackCooldown--;

		attackBoxCooldown--;
		if (attackBoxCooldown < 0)
		{
			attack1 = false;
			attack2 = false;
		}

		hitCooldown --;
	}

	void Update()
	{
		//get x - axis (PC controls)
		//move = 0;
		if (Input.GetKey (KeyCode.A))
			leftPressed ();
		else if (Input.GetKey (KeyCode.D))
			rightPressed ();

		if (grounded && Input.GetKeyDown (KeyCode.Space))
			jumpPressed ();
		if (Input.GetKeyDown (KeyCode.RightControl))
			attack1Pressed ();
	}

	public void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void leftPressed()
	{
		move = -1;
		movementCountdown = 5;
	}

	public void rightPressed()
	{
		move = 1;
		movementCountdown = 5;
	}

	public void movementReleased()
	{
		move = 0;
	}

	public void jumpPressed()
	{
		if (grounded && jumpDelay < 0)
		{
			if (hitCooldown < 0)
			{
				animArm.SetBool ("Ground", false);
				animLegs.SetBool ("Ground", false);
				animTorso.SetBool ("Ground", false);
				rigidbody2D.AddForce (new Vector2(0, jumpForce));
				jumpDelay = 10;
			}
		}
	}

	public void attack1Pressed()
	{
		if (attackCooldown < 0)
		{
			SoundEffectScript.Instance.PlayerSwingSound();
			float attackf = Random.Range (1, 3);
			int attack = (int)Mathf.Round (attackf);
			if (attack == 1) {
				animArm.SetTrigger ("attack1");
				animLegs.SetTrigger ("attack1");
				animTorso.SetTrigger ("attack1");
				attack1 = true;
			}
			else if (attack == 2) {
				animArm.SetTrigger ("attack2");
				animLegs.SetTrigger ("attack2");
				animTorso.SetTrigger ("attack2");
				attack2 = true;
			}
			attackCooldown = 20;
			attackBoxCooldown = 5;
		}
	}

	public void attack2Pressed()
	{
		attack2Button.guiTexture.texture = disabledTexture;
		activateWeapon (guiIndicator.activeGnome);
	}


	public void activateWeapon(int activeGnome)
	{
		var player = GameObject.Find ("Player");
		float x = transform.position.x;
		float y = transform.position.y;
		
		if(activeGnome == 1)
		{
			SoundEffectScript.Instance.LeafblowerSound();
			var leafblowerTransform = Instantiate (leafblowerPrefab) as Transform;
			Vector2 speed = new Vector2(15, 0);
			leafblowerTransform.position = new Vector2(transform.position.x - 5, transform.position.y);
			leafblowerTransform.rigidbody2D.velocity = new Vector2(speed.x, speed.y);
			Destroy (leafblowerTransform.gameObject, 0.8f);
		}
		else if (activeGnome == 3)
		{
			SoundEffectScript.Instance.LawnmowerSound();
			var lawnmowerTransform = Instantiate (lawnmowerPrefab) as Transform;
			Vector2 speed = new Vector2(15, 0);
			lawnmowerTransform.position = new Vector2(transform.position.x - 5, transform.position.y);
			lawnmowerTransform.rigidbody2D.velocity = new Vector2(speed.x, speed.y);
			Destroy (lawnmowerTransform.gameObject, 0.8f);
		}
		else if (activeGnome == 2)
		{
			SoundEffectScript.Instance.PitchforkSound();
			var pitchforkTransform = Instantiate (pitchforkPrefab) as Transform;
			pitchforkTransform.position = new Vector2(transform.position.x, transform.position.y + 5);
		}
		
	}
}
