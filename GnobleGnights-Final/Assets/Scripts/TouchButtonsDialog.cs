﻿using UnityEngine;
using System.Collections;

public class TouchButtonsDialog : MonoBehaviour {

	private DialogInfo dialogInfo;
	private GUIindicator guiIndicator;
	
	void Start()
	{
		dialogInfo = GameObject.FindObjectOfType (typeof(DialogInfo)) as DialogInfo;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
		guiIndicator.mainMenu = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		//updating the cooldown
		dialogInfo.buttonCooldown --;

		//handling input
		if (Input.touches.Length > 0)
		{
			if (dialogInfo.buttonCooldown < 0)
			{
				//loop through all the toucheson screen
				for (int i = 0; i < Input.touchCount; i++)
				{
					//executes this code for the current touch (i) on screen
					if (this.guiTexture.HitTest(Input.GetTouch (i).position))
					{
						string name = this.name;

						//if current touch hits our gui texture, run this code
						if (Input.GetTouch(i).phase == TouchPhase.Began)
						{
							
						}
						
						//letting go of a button
						if (Input.GetTouch (i).phase == TouchPhase.Ended)
						{
							if (Application.loadedLevelName.Equals ("gameIntro"))
							{
								if (name.Equals ("Skip"))
									Application.LoadLevel ("level1Intro");
								else if (name.Equals ("Next"))
								{
									Application.LoadLevel ("level1Intro");
								}
							}
							else if (Application.loadedLevelName.Equals ("level1Intro"))
							{
								if (name.Equals ("Skip"))
									Application.LoadLevel ("charSelect");
								else if (name.Equals ("Next"))
								{
									Application.LoadLevel ("charSelect");
								}
							}
							else if (Application.loadedLevelName.Equals ("level2Intro"))
							{
								if (name.Equals ("Skip"))
									Application.LoadLevel ("level2");
								else if (name.Equals ("Next"))
								{
									Application.LoadLevel ("level2");
								}
							}
							else if (Application.loadedLevelName.Equals ("level3Intro"))
							{
								if (name.Equals ("Skip"))
									Application.LoadLevel ("level3");
								else if (name.Equals ("Next"))
								{
									Application.LoadLevel ("level3");
								}
							}

							else if (Application.loadedLevelName.Equals ("bossLevelIntro"))
							{
								if (name.Equals ("Skip"))
									Application.LoadLevel ("levelBoss");
								else if (name.Equals ("Next"))
								{
									Application.LoadLevel ("levelBoss");
								}
							}
							else if (Application.loadedLevelName.Equals ("tutorialLevel"))
							{
								if (name.Equals ("Next"))
								{
									TutorialScript tut = GameObject.FindObjectOfType (typeof(TutorialScript)) as TutorialScript;
									tut.countdown = tut.delay * 60 * (tut.tips - tut.currentSlide);
								}										
							}
						}
					}
				}
			}
		}
	}
}
