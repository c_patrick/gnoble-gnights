﻿using UnityEngine;
using System.Collections;

//specifically for the main menu
public class TouchMenuButtons : MonoBehaviour {

	private GUIindicator guiIndicator;

	void Start()
	{
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	
	void Update () 
	{
		guiIndicator.paused = false;
		guiIndicator.inGame = false;
		guiIndicator.mainMenu = true;

		if (guiIndicator.mainMenu == true)
		{
			//updating the cooldown
			guiIndicator.menuCooldown --;

			//displaying confirmation if player chooses to quit
			if (guiIndicator.confirm)
			{
				if (this.name.Equals ("sure") || this.name.Equals ("yes") || this.name.Equals ("no"))
					this.guiTexture.enabled = true;
			}
			else
			{
				if (this.name.Equals ("sure") || this.name.Equals ("yes") || this.name.Equals ("no"))
					this.guiTexture.enabled = false;
			}

			//displaying options menu if it is activated
			if (guiIndicator.options)
			{
				if (this.name.Equals ("return"))
					this.guiTexture.enabled = true;
				else if (this.name.Equals ("musicOn"))
				{
					if (guiIndicator.music)
						this.guiTexture.enabled = true;
					else
						this.guiTexture.enabled = false;
				}
				else if (this.name.Equals ("musicOff"))
				{
					if (!guiIndicator.music)
						this.guiTexture.enabled = true;
					else
						this.guiTexture.enabled = false;
				}
				else if (this.name.Equals ("soundOn"))
				{
					if (guiIndicator.sound)
						this.guiTexture.enabled = true;
					else
						this.guiTexture.enabled = false;
				}
				else if (this.name.Equals ("soundOff"))
				{
					if (!guiIndicator.sound)
						this.guiTexture.enabled = true;
					else
						this.guiTexture.enabled = false;
				}
			}
			else
			{
				if (this.name.Equals ("musicOn") || this.name.Equals ("musicOff") || this.name.Equals ("soundOff") || this.name.Equals ("soundOn") || this.name.Equals ("return"))
					this.guiTexture.enabled = false;
			}

			//is there a touch on screen?
			if (Input.touches.Length <= 0)
			{
				//if no touches then execute this code
				
			}
			else //if there is a touch
			{
				if (guiIndicator.menuCooldown < 0)
				{
					//loop through all the toucheson screen
					for (int i = 0; i < Input.touchCount; i++)
					{
						//executes this code for the current touch (i) on screen
						if (this.guiTexture.HitTest(Input.GetTouch (i).position))
						{
							string name = this.name;

							//code here to change appearance of button if it is being touched
							//not sure how to do this with guiTextures

							//if current touch hits our gui texture, run this code
							if (Input.GetTouch(i).phase == TouchPhase.Began)
							{

							}
							
							//letting go of a button
							if (Input.GetTouch (i).phase == TouchPhase.Ended)
							{
								if (guiIndicator.confirm)
								{
									if (name.Equals ("yes"))
									{
										if (guiIndicator.sound)
											PlayerPrefs.SetInt ("sound", 1);
										else
											PlayerPrefs.SetInt ("sound", 0);
										if (guiIndicator.music)
											PlayerPrefs.SetInt ("music", 1);
										else
											PlayerPrefs.SetInt ("music", 0);
										Application.Quit();
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("no"))
									{
										guiIndicator.confirm = false;
										guiIndicator.menuCooldown = 30;
									}
								}
								else if (guiIndicator.options)
								{
									if (name.Equals ("musicOn"))
									{
										guiIndicator.music = !guiIndicator.music;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("musicOff"))
									{
										guiIndicator.music = !guiIndicator.music;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("soundOn"))
									{
										guiIndicator.sound = !guiIndicator.sound;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("soundOff"))
									{
										guiIndicator.sound = !guiIndicator.sound;
										guiIndicator.menuCooldown = 30;
									}
									else if (name.Equals ("return"))
									{
										guiIndicator.options = false;
										guiIndicator.menuCooldown = 30;
										//should probably save sound settings to file at this point as the player could exit just after changing them here
									}
								}
								else
								{
									if (name.Equals ("newGame"))
									{
										//guiIndicator.mainMenu = false;
										guiIndicator.inGame = false;
										guiIndicator.options = false;
										guiIndicator.paused = false;
										guiIndicator.confirm = false;
										PlayerPrefs.SetString ("level", "level1");
										guiIndicator.currentLevel = "level1";
										guiIndicator.activeGnome = 1;
										guiIndicator.playerDamage = 1;
										Application.LoadLevel ("gameIntro");
									}
									
									if (name.Equals ("resumeGame"))
									{
										//guiIndicator.mainMenu = false;
										guiIndicator.inGame = true;
										guiIndicator.options = false;
										guiIndicator.paused = false;
										guiIndicator.confirm = false;
										Application.LoadLevel (guiIndicator.currentLevel);
									}
									//going to need to load save here, unless all saved data is loaded at start of app
									
									//need to make a pop up menu here
									if (name.Equals ("options"))
									{
										guiIndicator.options = true;
										guiIndicator.menuCooldown = 30;
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if (this.name.Equals ("newGame") || this.name.Equals ("options") || this.name.Equals ("resumeGame"))
				this.guiTexture.enabled = false;
		}
	}
}
