﻿using UnityEngine;
using System.Collections;

public class FPSscript : MonoBehaviour {

	//calculating fps
	public float updateInterval = 0.5f;
	private float accum = 0;
	private int frames = 0;
	private float timeLeft;
	private float fps = 0;

	// Use this for initialization
	void Start () {
		timeLeft = updateInterval;
	}
	
	// Update is called once per frame
	void Update () {
		//fps
		timeLeft -= Time.deltaTime;
		accum += Time.timeScale / Time.deltaTime;
		++frames;
		
		if (timeLeft <= 0.0f)
		{
			fps = accum / frames;
			timeLeft = updateInterval;
			accum = 0.0f;
			frames = 0;
		}

		GUIText fpsText = this.GetComponent<GUIText> ();
		fps = (int)fps;
		fpsText.text = "" + fps;
	}
}
