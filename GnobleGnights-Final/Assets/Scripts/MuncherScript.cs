﻿using UnityEngine;
using System.Collections;

public class MuncherScript : MonoBehaviour {

	//stats
	public int health = 10;
	public int damage = 1;
	public int attackDamage = 2;
	public int seedValue = 30;
	public float maxSpeed = 2f;
	public float sightRange = 3f; //2 for standard platforms, 5.5 for standard ground blocks (complete range - might want enemy view range to be smaller)
	public float attackRange = 3f; //will vary for every enemy (not every creeper), public so that it can be easily changed without opening the script editor
	
	//direction
	private float direction = -1f;
	
	//animator
	public Animator anim;
	
	//cooldown
	public int attackCooldown = 60;
	private int attackBoxCooldown = 30;
	public int dirChangeCooldown = 20;
	public int hitCooldown = 0;
	
	//playerObejct
	PlayerScript playerScript;

	//player in sight
	public bool playerSpotted = false;
	
	// Use this for initialization
	void Start () {
		//initialise animator here
		anim = transform.Find ("MuncherSprite").GetComponent<Animator> ();
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}

	//update here
	void FixedUpdate () {
		
		//updating cooldowns
		attackCooldown --;
		dirChangeCooldown --;
		hitCooldown --;
		attackBoxCooldown --;
		
		//changing layer if hit so they don't hit boundaries (so you can knock them off platforms)
		if (hitCooldown > 0)
			gameObject.layer = 14;
		else
			gameObject.layer = 12;
		
		//enabling/disabling attack boxes
		if (attackBoxCooldown >= 0 && attackBoxCooldown < 5 && hitCooldown < 0)
			transform.FindChild ("MuncherAttackBox").GetComponent<CircleCollider2D> ().enabled = true;
		else
			transform.FindChild ("MuncherAttackBox").GetComponent<CircleCollider2D> ().enabled = false;
		
		//check if in range of attack
		if (Mathf.Abs (playerScript.transform.position.x - this.transform.position.x) < attackRange && Mathf.Abs ((playerScript.transform.position.y - 0.17f) - this.transform.position.y) < 1.2f)
		{
			if (attackCooldown < 0)
			{
				anim.SetTrigger("attack");
				attackBoxCooldown = 30;
				attackCooldown = 60;
			}
		}
		//else move
		else
		{
			//don't move if falling
			if (transform.rigidbody2D.velocity.y == 0)
			{
				//if hit cooldown has expired
				if (hitCooldown < 0)
				{
					//moving the creeper
					rigidbody2D.velocity = new Vector2 (direction * maxSpeed, rigidbody2D.velocity.y);
				}
			}
		}

		//if the enemy is on screen then check for player distance
		if (transform.Find ("MuncherSprite").renderer.isVisible) 
		{
			//if the player is within range of sight on the x axis
			if (Mathf.Abs(playerScript.transform.position.x - this.transform.position.x) < sightRange)
			{
				//if the player is within range of sight on the y axis
				if (Mathf.Abs ((playerScript.transform.position.y - 0.17f) - this.transform.position.y) < 0.25f)
				{
					//change direction to face player
					if (playerScript.transform.position.x < this.transform.position.x)
					{
						if (direction != -1)
						{
							direction = -1;
							Flip ();
						}
					}
					else
					{
						if (direction != 1)
						{
							direction = 1;
							Flip ();
						}
					}
					//set player spotted bool to true
					playerSpotted = true;
				}
				else
				{
					//set player spotted bool to false
					playerSpotted = false;
				}
			}
			else
			{
				//set player spotted bool to false
				playerSpotted = false;
			}
		}
		//destroying object if health is too low
		if (health <= 0 || transform.position.y < -4)
		{
			SeedEffectScript.Instance.SeedEffect(transform.position);
			//DestroyObject (transform.FindChild ("MuncherAttackBox"));
			DestroyObject (this);
			if (health <= 0)
				playerScript.seedCount += seedValue;
		}
	}

	public void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	public void changeDirection()
	{
		direction *= -1;
		Flip ();
		dirChangeCooldown = 50;
	}
}
