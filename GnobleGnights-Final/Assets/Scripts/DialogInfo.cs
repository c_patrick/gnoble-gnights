﻿using UnityEngine;
using System.Collections;

public class DialogInfo : MonoBehaviour {

	public int slideNum = 1;
	public int slideCount = 1;
	public int buttonCooldown = 30;
	//slides
	public GameObject slide1;
	//public GameObject slide2;

	void Start()
	{
		//create sprites for all slides
		slide1 = GameObject.Find ("Slide1");
	}
	
	// Update is called once per frame
	void Update () {

		if (Application.loadedLevelName != "tutorialLevel")
		{			
			//updating the cooldown
			buttonCooldown --;

			// making the slide visible
			slide1.renderer.enabled = true;
		}
	}
}
