﻿using UnityEngine;
using System.Collections;

public class BossScript : MonoBehaviour {
	private BossWeaponScript weapon;
	private PlayerScript player;
	public Transform keyPrefab = null;

	private int bossMiniAttackCoolDown = 0;
	private int bossMiniCount = 0;
	public int bossMiniAttackTime = 200;
	public int bossMiniAttackDelay = 30;
	public int bossMiniPerAttack = 2;

	private int vineAttackCoolDown = 0;
	private int vineWarningCoolDown = 0;
	private int vineAttackCount = 0;
	public int vineAttackTime = 500;
	public int vineAttackDelay = 30;
	public int vinePerAttack = 10;
	public int vineAttackWarning = 50;

	private int thornAttackCoolDown = 0;
	private int thornWarningCoolDown = 0;
	private int thornAttackCount = 0;
	public int thornAttackTime = 900;
	public int thornAttackDelay = 30;
	public int thornPerAttack = 25;
	public int thornAttackWarning = 50;

	public bool dead = false;
	// boss damage
	public int damage = 1;
	public float health = 50;

	GUIindicator guiIndicator;

	void Start () 
	{
		player = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	// Use this for initialization
	void Awake() {
		weapon = GetComponent<BossWeaponScript>();
	}

	void FixedUpdate () {
		// disable boss attack if player is dead
		if (player.health <= 0 || player.transform.position.y < -10) {
			this.enabled = false;
		}

		//checking music
		if (!guiIndicator.music)
		{
			// mute boss level music
			GameObject.Find("MusicBossLevel").audio.mute = true;
		}
		else
		{
			if (health <= 0)
			{
				// mute boss level music
				GameObject.Find("MusicBossLevel").audio.mute = true;
			}
			else
			{
				// mute boss level music
				GameObject.Find("MusicBossLevel").audio.mute = false;
			}
		}

		// if boss is dead
		if (health <= 0) {
			// spray death particle
			SpecialEffectsHelper.Instance.BossDeathEffect(transform.position);
			// set death sprite
			GetComponent<Animator>().SetBool("death", true);
			// disable player hit box
			player.GetComponentInChildren<playerHitBoxScript>().collision = false;

			// allow access to the prison cell
			GameObject.Find("PrisonAccess").collider2D.enabled = false;
			// drop key
			var keyTransform = Instantiate(keyPrefab) as Transform;
			keyTransform.position = transform.position;

			this.enabled = false;
		}
		else {
			bossMiniAttack();
			vineAttack();
			thornAttack();
		}
	}
	

	void bossMiniAttack() {
		// if attack time, spawn mini boss
		if (bossMiniAttackCoolDown >= bossMiniAttackTime) {
			weapon.attack("BOSSMINI");
			// keep spawning mini boss at a certain rate
			bossMiniAttackCoolDown = bossMiniAttackTime - bossMiniAttackDelay;
			// keep track of mini boss count
			++bossMiniCount;
			
			// if all the mini bosses have been spawned, reset cool down and counter
			if (bossMiniCount >= bossMiniPerAttack) {
				bossMiniAttackCoolDown = -1;
				bossMiniCount = 0;
			}
		}
		++bossMiniAttackCoolDown;
	}

	void vineAttack() {
		// if vine attack time
		if (vineAttackCoolDown >= vineAttackTime) {
			// give vine attack warning
			if (vineWarningCoolDown <= 5)
				weapon.warning(5 - vineWarningCoolDown);
			
			// attack if warning time is finished
			else if (vineWarningCoolDown >= vineAttackWarning) {
				weapon.attack("VINEATTACK");
				// keep attacking at a certain rate
				vineAttackCoolDown = vineAttackTime - vineAttackDelay;
				// keep track of attack count
				++vineAttackCount;
				
				// if the attack is finished, reset cool down and counter
				if (vineAttackCount >= vinePerAttack) {
					vineAttackCoolDown = -1;
					vineAttackCount = 0;
					vineWarningCoolDown = -1;
				}
			}
			++vineWarningCoolDown;
		}
		++vineAttackCoolDown;
	}

	void thornAttack() {
		// if thorn attack time
		if (thornAttackCoolDown >= thornAttackTime) {
			transform.GetComponent<Animator>().SetBool("thornAttack", true);
			// attack if warning time is finished
			if (thornWarningCoolDown >= thornAttackWarning) {
				weapon.attack("THORNATTACK");
				// keep attacking at a certain rate
				thornAttackCoolDown = thornAttackTime - thornAttackDelay;
				// keep track of attack count
				++thornAttackCount;
				
				// if the attack is finished, reset cool down and counter
				if (thornAttackCount >= thornPerAttack) {
					thornAttackCoolDown = -1;
					thornAttackCount = 0;
					thornWarningCoolDown = -1;
				}
			}
			++thornWarningCoolDown;
		}
		else
			transform.GetComponent<Animator>().SetBool("thornAttack", false);

		++thornAttackCoolDown;
	}
}
