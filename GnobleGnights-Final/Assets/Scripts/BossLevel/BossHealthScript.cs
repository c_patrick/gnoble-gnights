﻿using UnityEngine;
using System.Collections;

public class BossHealthScript : MonoBehaviour {

	// boss health
	private float maxHealth;
	private float currentHealth;
	private float tempHealth;
	private float healthBarLength;

	private GUITexture healthBarGUITexture;
	private GUITexture bossIconGUITexture;
	private BossScript bossScript;

	private Rect healthBar;
	private int visibilityCoolDown = 0;


	void Start() {
		bossScript = GameObject.FindObjectOfType (typeof(BossScript)) as BossScript;
		healthBarGUITexture = this.gameObject.GetComponent ("GUITexture") as GUITexture;
		bossIconGUITexture = GameObject.Find("BossIcon").GetComponent ("GUITexture") as GUITexture;

		// get boss health
		maxHealth = bossScript.health;
		currentHealth = maxHealth;
		tempHealth = currentHealth;
		// get current healthbar size
		healthBar = healthBarGUITexture.pixelInset;
	}

	// Update is called once per frame
	void Update () {
		// get boss current health
		currentHealth = bossScript.health;
		// disable health bar when boss is dead
		if (currentHealth <= 0) {
			// disable boss icon
			bossIconGUITexture.enabled = false;
			// disable health bar
			healthBarGUITexture.enabled = false;
		}

		// if health decreased
		else if (tempHealth != currentHealth) {
			// enable healthbar visibility when boss is attacked
			if (!bossIconGUITexture.enabled) {
				bossIconGUITexture.enabled = true;
				healthBarGUITexture.enabled = true;
			}

			// get health percentage
			float healthPercent = (currentHealth / maxHealth);

			// reduce health bar width
			healthBarGUITexture.pixelInset = new Rect(0, 0, healthBar.width * healthPercent, healthBar.height);
			tempHealth = currentHealth;

			// reset health bar visibility cooldown
			visibilityCoolDown = 0;
		}
		// if boss is not getting hit for some time...
		else if (visibilityCoolDown > 50 && bossIconGUITexture.enabled) {
			// disable boss icon
			bossIconGUITexture.enabled = false;
			// disable health bar
			healthBarGUITexture.enabled = false;
		}
		++visibilityCoolDown;
	}
}
