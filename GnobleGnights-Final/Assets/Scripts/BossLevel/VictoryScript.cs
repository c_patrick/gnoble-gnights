﻿using UnityEngine;
using System.Collections;

public class VictoryScript : MonoBehaviour {
	
	GUIindicator guiIndicator;

	void Start() {
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}


	void Update() {
		if (guiIndicator.victory)
			transform.GetComponent<BoxCollider2D>().enabled = true;
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Application.LoadLevel("victoryScene");
	}
}
