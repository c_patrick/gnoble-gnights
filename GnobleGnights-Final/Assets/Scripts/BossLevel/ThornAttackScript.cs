﻿using UnityEngine;
using System.Collections;

public class ThornAttackScript : MonoBehaviour {

	// thorn damage
	public int damage = 1;

	// Use this for initialization
	void Start () {
		Destroy(gameObject, 5);
	}

	void OnCollisionEnter2D (Collision2D collision) {
		Destroy(gameObject);
	}
}
