﻿using UnityEngine;
using System.Collections;

public class VineBoundaryScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D vine) {
		// stop the vine from going further
		if (vine.tag == "BossVine") {
			var bossObject = GameObject.Find("Boss");
			// get boss direction
			float xDir = bossObject.transform.position.x - transform.position.x;
			float yDir = bossObject.transform.position.y - transform.position.y;
			Vector2 direction = new Vector2(xDir - 0.3f, yDir);
			//direction.Normalize();
			vine.transform.rigidbody2D.velocity = direction;
		}

		if (vine.tag == "BossVineAttack")
			vine.transform.rigidbody2D.velocity = new Vector2(0, -5);
	}
}
