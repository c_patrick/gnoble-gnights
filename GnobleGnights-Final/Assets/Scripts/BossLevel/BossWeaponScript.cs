using UnityEngine;
using System.Collections;

/// <summary>
/// Launch projectiles
/// </summary>
public class BossWeaponScript : MonoBehaviour {

	/// <summary>
	/// mini boss prefab
	/// </summary>
	public Transform bossMiniPrefab = null;
	public Transform vineWarningPrefab = null;
	public Transform vineAttackPrefab = null;
	public Transform thornAttackPrefab = null;

	private bool flag = true;
	private int angle = 20;
	private float xForce = 0;
	private float yForce = 0;

	/// <summary>
	/// Create a new projectile if possible
	/// </summary>
	public void attack (string attackType) {
		if (attackType == "BOSSMINI") {
			// create mini boss object
			var bossMiniTransform = Instantiate(bossMiniPrefab) as Transform;
					
			// Assign position
			bossMiniTransform.position = transform.position;
		}
		else if (attackType == "THORNATTACK") {
			if (angle <= -220)
				angle = 20;

			var thornAttackTrasform = Instantiate(thornAttackPrefab) as Transform;
			thornAttackTrasform.position = transform.position;
			thornAttackTrasform.Rotate(0, 0, angle);
			
			xForce = -Mathf.Sin((90 + angle) * Mathf.Deg2Rad);
			yForce = -Mathf.Sin(angle * Mathf.Deg2Rad);

			Vector2 force = new Vector2(xForce, yForce);
			thornAttackTrasform.rigidbody2D.velocity = force.normalized * 10;

			//print ("angle:" + angle + " x: " + (-Mathf.Sin(90 + angle)) + " y: " + (-Mathf.Sin(angle)));

			// rotate 10 degrees
			angle -= 10;
		}
		else if (attackType == "VINEATTACK") {
			// create vine object
			var vineAttackTrasform = Instantiate(vineAttackPrefab) as Transform;

			// randomly generate vine attack positions
			float randLeft = Random.Range(-10.0f, -1.7f);
			float randRight = Random.Range(4.5f, 12.8f);
			// switch between left side attack and right side attack
			if (flag) {
				vineAttackTrasform.position = new Vector2(randLeft, -10);
				flag = false;
			}
			else {
				vineAttackTrasform.position = new Vector2(randRight, -10);
				flag = true;
			}
		}
	}

	public void warning(int vineCount) {
		// create vine object
		var vineWarningTransform = Instantiate(vineWarningPrefab) as Transform;
		// set position of the vines to boss
		vineWarningTransform.position = new Vector2(transform.position.x, transform.position.y);
		// rotate each vine
		if (vineCount == 5) {
			vineWarningTransform.Rotate(0, 0, 45);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(300, -10));
		}
		else if (vineCount == 4) {
			vineWarningTransform.Rotate(0, 0, 20);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(150, -10));
		}
		else if (vineCount == 3) {
			vineWarningTransform.Rotate(0, 0, 10);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(50, -10));
		}
		else if (vineCount == 2) {
			vineWarningTransform.Rotate(0, 0, -10);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(-50, -10));
		}
		else if (vineCount == 1) {
			vineWarningTransform.Rotate(0, 0, -20);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(-150, -10));
		}
		else {
			vineWarningTransform.Rotate(0, 0, -40);
			vineWarningTransform.rigidbody2D.AddForce(new Vector2(-300, -10));
		}
	}
}


