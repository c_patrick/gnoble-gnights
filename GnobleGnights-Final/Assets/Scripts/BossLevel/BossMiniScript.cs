﻿using UnityEngine;
using System.Collections;

/// <summary>
/// MiniBoss
/// </summary>
public class BossMiniScript : MonoBehaviour {

	// boss mini damage
	public int damage = 1;
	public bool alive = true;
	private Animator anim;
	public Vector2 speed = new Vector2(3, 3);

	void Start() {
		Destroy(gameObject, 30);
		anim = transform.GetComponent<Animator>();
	}

	// Update is called once per frame
	void FixedUpdate() {
		if (alive)
			moveTowardsPlayer();
	}

	void moveTowardsPlayer() {
		var playerObject = GameObject.Find("Player");
		// get player direction
		float xDir = playerObject.transform.position.x - transform.position.x;
		float yDir = playerObject.transform.position.y - transform.position.y;
		
		// if player is facing left, activate left movement animation
		if (xDir < 0) {
			anim.SetBool("right", false);
		}
		// otherwise activate right movement animation
		else {
			anim.SetBool("right", true);
		}

		Vector2 direction = new Vector2(xDir, yDir);
		direction.Normalize();
		Vector3 movement = new Vector3(direction.x * speed.x, direction.y * speed.y, 0);
		movement *= Time.deltaTime;
		transform.Translate(movement);
	}
}
