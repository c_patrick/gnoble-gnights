﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour {

	private PlayerScript playerScript;

	// Use this for initialization
	void Start () {
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.position = new Vector2 (transform.position.x + 0.05f, transform.position.y);

		if (transform.position.x - playerScript.transform.position.x > 10)
				transform.position = new Vector2 (transform.position.x - 20, transform.position.y);
		else if (transform.position.x - playerScript.transform.position.x < -10)
				transform.position = new Vector2 (transform.position.x + 20, transform.position.y);
	}
}
