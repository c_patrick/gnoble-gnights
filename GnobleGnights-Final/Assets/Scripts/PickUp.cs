﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {
	//private PlayerScript playerScript;
	private int cooldown = 0;

	public GUITexture attack2Button;
	public Texture2D enabledTexture;

	private GUIindicator guiIndicator;


	// Use this for initialization
	void Start () {
		//guiIndicator
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		cooldown--;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (cooldown < 0)
		{
			if (other.tag == "Player")
			{
				SoundEffectScript.Instance.PickUpSound ();
				if ((this.tag == "Heart") && other.GetComponent<PlayerScript>().health <= 6)
				{
					if (other.GetComponent<PlayerScript>().health < 6)
					{
						other.GetComponent<PlayerScript>().health += 2;
						if (other.GetComponent<PlayerScript>().health > 6)
							other.GetComponent<PlayerScript>().health = 6;
					}
					Handheld.Vibrate ();
					Destroy(this.gameObject);
				}
				else if(this.tag == "PowerUp")
				{
					attack2Button.guiTexture.texture = enabledTexture;
					Destroy(this.gameObject);
				}
			}
		}
	}
}
