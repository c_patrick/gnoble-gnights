﻿using UnityEngine;
using System.Collections;

public class SoundEffectScript : MonoBehaviour {
	
	/// <summary>
	/// Singleton
	/// </summary>
	public static SoundEffectScript Instance;
	
	public AudioClip splashSound;
	public AudioClip pickUpSound;
	public AudioClip playerHitSound;
	public AudioClip playerSwingSound;
	public AudioClip playerAttackSound;
	public AudioClip leafblowerSound;
	public AudioClip pitchforkSound;
	public AudioClip lawnmowerSound;

	private GUIindicator guiIndicator;

	void Start() {
		//guiIndicator
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	
	void Awake()
	{
		// Register the singleton
		if (Instance != null)
		{
			Debug.LogError("Multiple instances of SoundEffectsHelper!");
		}
		Instance = this;
	}
	
	public void SplashSound()
	{
		MakeSound(splashSound);
	}
	
	public void PickUpSound()
	{
		MakeSound(pickUpSound);
	}
	
	public void PlayerHitSound()
	{
		MakeSound(playerHitSound);
	}

	public void PlayerSwingSound()
	{
		MakeSound(playerSwingSound);
	}
	
	public void PlayerAttackSound()
	{
		MakeSound(playerAttackSound);
	}

	public void LeafblowerSound()
	{
		MakeSound(leafblowerSound);
	}
	
	public void PitchforkSound()
	{
		MakeSound(pitchforkSound);
	}
	
	public void LawnmowerSound()
	{
		MakeSound(lawnmowerSound);
	}
	
	/// <summary>
	/// Play a given sound
	/// </summary>
	/// <param name="originalClip"></param>
	private void MakeSound(AudioClip originalClip)
	{
		if (guiIndicator.sound)
			AudioSource.PlayClipAtPoint(originalClip, transform.position);
	}
}
