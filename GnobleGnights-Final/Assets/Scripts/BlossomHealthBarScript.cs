﻿using UnityEngine;
using System.Collections;

public class BlossomHealthBarScript : MonoBehaviour {
	
	// blossom health
	public float maxHealth = 300;
	public float currentHealth;
	private float healthBarLength;
	private float countRate = 0;
	
	private GUITexture healthBarGUITexture;
	private GUITexture blossomIconGUITexture;
	private GUITexture blossomDeadIconGUITexture;

	GUIindicator guiIndicator;
	
	private Rect healthBar;
	
	
	void Start() {
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
		healthBarGUITexture = this.gameObject.GetComponent ("GUITexture") as GUITexture;
		blossomIconGUITexture = GameObject.Find("BlossomIcon").GetComponent ("GUITexture") as GUITexture;
		blossomDeadIconGUITexture = GameObject.Find("BlossomIconDead").GetComponent ("GUITexture") as GUITexture;

		currentHealth = maxHealth;

		// get current healthbar size
		healthBar = healthBarGUITexture.pixelInset;
	}

	void FixedUpdate() {
		if (!guiIndicator.defeat)
		{
			if (countRate >= 60) {
				currentHealth -= 1;
				countRate = 0;
			}
			++countRate;
		}
	}


	// Update is called once per frame
	void Update () {

		// disable health bar when boss is dead
		if (currentHealth <= 0) {
			//defeat
			guiIndicator.defeat = true;
			guiIndicator.checkPointUsed = false;
			// disable blossom icon
			//blossomIconGUITexture.enabled = false;
			// disable health bar
			healthBarGUITexture.enabled = false;
			blossomIconGUITexture.enabled = false;
			blossomDeadIconGUITexture.enabled = true;
		}
		
		// get health percentage
		float healthPercent = (currentHealth / maxHealth);
		
		// reduce health bar width
		healthBarGUITexture.pixelInset = new Rect(0, 0, healthBar.width * healthPercent, healthBar.height);
	}
}
