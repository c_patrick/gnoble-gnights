﻿using UnityEngine;
using System.Collections;

public class SpecialAttackScript : MonoBehaviour {

	private BossScript bossScript;
	GUIindicator guiIndicator;
	
	// Use this for initialization
	void Start () {
		bossScript = GameObject.FindObjectOfType (typeof(BossScript)) as BossScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}
	

	void OnTriggerEnter2D(Collider2D enemy)
	{
		if (enemy.name == "Creeper")
		{
			if (enemy.GetComponent<CreeperScript>() != null)
			{
				if (enemy.GetComponent<CreeperScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));
					enemy.GetComponent<CreeperScript>().hitCooldown = 20;
					enemy.GetComponent<CreeperScript>().health = 0;
					
					//interupting their attack
					enemy.GetComponent<CreeperScript>().anim.SetTrigger ("hit");
				}
			}
		}
		else if (enemy.name == "Muncher")
		{
			if (enemy.GetComponent<MuncherScript>() != null)
			{
				if (enemy.GetComponent<MuncherScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (100, 280));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-100, 280));
					enemy.GetComponent<MuncherScript>().hitCooldown = 10;
					enemy.GetComponent<MuncherScript>().health = 0;
					
					//interupting their attack
					//enemy.GetComponent<MuncherScript>().anim.SetTrigger ("hit");
				}
			}
		}
		else if (enemy.name == "Sunflower")
		{
			if (enemy.GetComponent<SunShooterScript>() != null)
			{
				if (enemy.GetComponent<SunShooterScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (100, 280));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-100, 280));
					enemy.GetComponent<SunShooterScript>().hitCooldown = 10;
					enemy.GetComponent<SunShooterScript>().health = 0;
				}
			}
		}
		else if (enemy.name == "seedShot(Clone)")
		{
			if (enemy.GetComponent<ShotScript>() != null)
			{
				if (enemy.GetComponent<ShotScript>().cooldown < 0)
				{
					enemy.GetComponent<ShotScript>().Flip ();
					enemy.GetComponent<ShotScript>().direction *= -1;
					enemy.GetComponent<ShotScript>().cooldown = 20;
				}
				//DestroyObject (enemy.gameObject);
			}
		}
		else if (enemy.name == "Tulip")
		{
			if (enemy.GetComponent<TulipScript>() != null)
			{
				if (enemy.GetComponent<TulipScript>().hitCooldown < 0)
				{
					//making them bounce
					enemy.rigidbody2D.velocity = new Vector2(0, 0);
					if (this.transform.position.x < enemy.transform.position.x)
						enemy.rigidbody2D.AddForce (new Vector2 (250, 330));
					else
						enemy.rigidbody2D.AddForce (new Vector2 (-250, 330));
					enemy.GetComponent<TulipScript>().hitCooldown = 20;
					enemy.GetComponent<TulipScript>().health = 0;
					
					//interupting their attack
					enemy.GetComponent<TulipScript>().anim.SetTrigger ("hit");
				}
			}
		}
	}
}
