﻿using UnityEngine;
using System.Collections;

public class EndLevelScript : MonoBehaviour {

	private PlayerScript player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player").GetComponent<PlayerScript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if ((player.transform.position.x + 8) >= transform.position.x) {
			transform.rigidbody2D.AddForce(new Vector2(3, 0));
		}
	}
}
