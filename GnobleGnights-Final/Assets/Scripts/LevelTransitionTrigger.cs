﻿using UnityEngine;
using System.Collections;

public class LevelTransitionTrigger : MonoBehaviour {

	private GUIindicator guiIndicator;
	private PlayerScript playerScript;

	void Start()
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
		guiIndicator = GameObject.FindObjectOfType (typeof(GUIindicator)) as GUIindicator;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		//check if player
		if (other.name == "Player")
		{
			print ("Player entered");
			guiIndicator.playerDamage = playerScript.damage;
			//check current level and load next one
			if (Application.loadedLevelName.Equals ("level1"))
			{
				//load level 2 intro
				Application.LoadLevel ("level2Intro");
			}
			else if (Application.loadedLevelName.Equals ("level2"))
			{
				//load level 3 intro
				Application.LoadLevel ("level3Intro");
			}
			else if (Application.loadedLevelName.Equals ("level3"))
			{
				//load boss level intro
				Application.LoadLevel ("bossLevelIntro");
			}
			else if(Application.loadedLevelName.Equals ("levelBoss"))
				Application.LoadLevel("victoryScene");
			else
				print (Application.loadedLevelName);
		}
	}
}
