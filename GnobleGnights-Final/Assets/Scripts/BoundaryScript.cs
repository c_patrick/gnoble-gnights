﻿using UnityEngine;
using System.Collections;

public class BoundaryScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//enemy trigger - change direction
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "Creeper")
		{
			if (!other.GetComponent<CreeperScript>().playerSpotted)
			{
				if (other.GetComponent<CreeperScript>().dirChangeCooldown < 0)
					other.GetComponent<CreeperScript>().changeDirection();
			}
		}
		else if (other.name == "Tulip")
		{
			if (!other.GetComponent<TulipScript>().playerSpotted)
			{
				if (other.GetComponent<TulipScript>().dirChangeCooldown < 0)
					other.GetComponent<TulipScript>().changeDirection ();
			}
		}
		else if (other.name == "Muncher")
		{
			if (!other.GetComponent<MuncherScript>().playerSpotted)
			{
				if (other.GetComponent<MuncherScript>().dirChangeCooldown < 0)
					other.GetComponent<MuncherScript>().changeDirection ();
			}
		}
	}
}
