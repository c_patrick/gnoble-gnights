﻿using UnityEngine;
using System.Collections;

public class healthScript : MonoBehaviour {

	private PlayerScript playerScript;
	
	// Use this for initialization
	void Start () 
	{
		playerScript = GameObject.FindObjectOfType (typeof(PlayerScript)) as PlayerScript;
	}
	
	// Update is called once per frame
	void Update () {

		string name = this.name;

		//cycle through heart names (if player can have more than 3 hearts more must be added)
		if (name == "heart0-5")
		{
			if (playerScript.health == 1)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
		else if (name == "heart1")
		{
			if (playerScript.health >= 2)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
		else if (name == "heart1-5")
		{
			if (playerScript.health == 3)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
		else if (name == "heart2")
		{
			if (playerScript.health >= 4)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
		else if (name == "heart2-5")
		{
			if (playerScript.health == 5)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
		else if (name == "heart3")
		{
			if (playerScript.health >= 6)
				this.guiTexture.enabled = true;
			else
				this.guiTexture.enabled = false;
		}
	}
}
